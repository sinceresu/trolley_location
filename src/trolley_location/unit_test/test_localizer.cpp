#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include<pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "src/trolley_localizer.h"


DEFINE_string(calibration_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(cloud_map_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;

namespace trolley_location {
namespace  object_location{
namespace {


std::shared_ptr<ObjectLocalizer> BuildPoseLocalizer( 
                    const std::string& calibration_filepath) {


auto  localizer = std::make_shared<TrolleyLocalizer>();
localizer->LoadCalibrationFile(calibration_filepath);
  return localizer;
}

void Run( ) {

  auto localizer = BuildPoseLocalizer(FLAGS_calibration_filepath);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_point_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>() );

    pcl::io::loadPCDFile(FLAGS_cloud_map_filepath, *input_point_cloud);

    localizer->SetCloudMap(input_point_cloud);
    localizer->SetParamters(0.1);
    localizer->SetCullingParamters(80.0, 
      80.0, 
      1, 
      50
    );

    cv::Rect2f pos2d = cv::Rect2f(1084.89, 529.693, 0, 0);
    cv::Point3f object_position;
    std::vector<cv::Point3f> object_area;

    localizer->Localize(pos2d,  object_position, object_area);

    LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // cv::Point2f image_position = localizer->Project3dPointToImage(object_position);
    // LOG(INFO) << "object 2d position: " << image_position.x << ","   << image_position.y;

    pos2d = cv::Rect2f(1071.39, 526.563, 0, 0);
    localizer->Localize(pos2d,  object_position, object_area);
    LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // pos2d = cv::Rect2f(652.183, 444.891, 0, 0);
    // localizer->Localize(pos2d,  object_position);
    // LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // pos2d = cv::Rect2f(651.485, 444.841, 0, 0);
    // localizer->Localize(pos2d,  object_position);
    // LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // //  image_position = localizer->Project3dPointToImage(object_position);
    // // LOG(INFO) << "object 2d position: " << image_position.x << ","   << image_position.y;

    // pos2d = cv::Rect2f(652.221, 444.999, 0, 0);
    // localizer->Localize(pos2d,  object_position);
    // LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // pos2d = cv::Rect2f(651.004, 444.814, 0, 0);
    // localizer->Localize(pos2d,  object_position);
    // LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // pos2d = cv::Rect2f(650.427, 444.325, 0, 0);
    // localizer->Localize(pos2d,  object_position);
    // LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;

    // pos2d = cv::Rect2f(650.794, 444.836, 0, 0);
    // localizer->Localize(pos2d,  object_position);
    // LOG(INFO) << "object 3d position: " << object_position.x << ","   << object_position.y << "," <<  object_position.z;


}
void Test(int argc, char** argv) {

    Run();

}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  CHECK(!FLAGS_calibration_filepath.empty())
      << "-configuration_directory is missing.";

  CHECK(!FLAGS_cloud_map_filepath.empty())
      << "-map_configuration_basename is missing.";


  trolley_location::object_location::Test(argc, argv);

}
