#pragma once

#include "object_localizer.h"

namespace trolley_location{

namespace object_location{
  
class TrolleyLocalizer  : public ObjectLocalizer
{
public:
  explicit TrolleyLocalizer() ;

  virtual ~TrolleyLocalizer();

  TrolleyLocalizer(const TrolleyLocalizer&) = delete;
  TrolleyLocalizer& operator=(const TrolleyLocalizer&) = delete;

private:
  virtual int GetOccupiedArea(const cv::Rect2f&  object_region, const cv::Point3f&object_center, std::vector<cv::Point3f>& object_occupied_area) override;


};
} // namespaceobject_location
}// namespaceobject_location
