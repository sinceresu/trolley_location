#pragma once

#include "object_localizer.h"

namespace trolley_location{

namespace object_location{
  
class PersonLocalizer  : public ObjectLocalizer
{
public:
  explicit PersonLocalizer() ;

  virtual ~PersonLocalizer();

  PersonLocalizer(const PersonLocalizer&) = delete;
  PersonLocalizer& operator=(const PersonLocalizer&) = delete;

private:
  virtual void GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position2d, double & object_height);
  virtual int GetOccupiedArea(const cv::Rect2f&  object_region, const cv::Point3f&object_center, std::vector<cv::Point3f>& object_occupied_area) override;

  bool IsWholeBody(const cv::Rect2f& object_region);
  bool IsAtBottom(const cv::Rect2f& object_region);
  

};
} // namespaceobject_location
}// namespaceobject_location
