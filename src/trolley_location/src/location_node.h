#pragma once

#include <map>
#include <memory>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "ros/ros.h"
#include "trolley_location_msgs/ObjectDetect.h"

namespace trolley_location {
namespace  object_location{
class ObjectLocalizer;
class LocationNode {
 public:
   struct LocationNodeOptions {
    std::string calibration_filepath;    
    std::string cloud_map_filepath;    
    std::string detect_topic;
    std::string location_topic;
    std::string frame_id;
    float horizontal_fov;
    float vertical_fov;
    float near_plane_distance;
    float far_plane_distance;
    std::vector<int> object_types;
    std::vector<std::string> object_labels;
    std::vector<float> object_heights;

  };

  LocationNode(const LocationNodeOptions& options);
  ~LocationNode();

  LocationNode(const LocationNode&) = delete;
  LocationNode& operator=(const LocationNode&) = delete;

  private:
    void LaunchSubscribers();
    void HandleDetectionMessage(const std::string& sensor_id,
                              const trolley_location_msgs::ObjectDetect::ConstPtr& msg);

    LocationNodeOptions node_options_;
    ::ros::NodeHandle node_handle_;
    ::ros::Publisher pose_position_publisher_;
    ::ros::Subscriber detect_subscriber_;
    std::map<int, std::shared_ptr<ObjectLocalizer>> pose_localizers_;


  };
  
}
}