#include "object_localizer.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include<pcl/io/pcd_io.h>


using namespace std;

namespace trolley_location{

namespace object_location{
namespace {
constexpr float kHorizontalFOV = 60.0f;
constexpr float kVerticalFOV = 60.0f;
constexpr float kNearPlaneDistance = 0.7f;
constexpr float kFarPlaneDistance = 15.0f;
constexpr int kMaxImagesInDistanceMap = 50;
constexpr float kOccludeDistanceTolerence = 0.02f;
constexpr float MIN_VALID_X =  numeric_limits<float>::min() + 1;
}


ObjectLocalizer::ObjectLocalizer() :
        horizontal_fov_(kHorizontalFOV),
        vertical_fov_(kVerticalFOV),
        near_plane_distance_(kNearPlaneDistance),
        far_plane_distance_(kFarPlaneDistance)
{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

ObjectLocalizer::~ObjectLocalizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}


int ObjectLocalizer::SetCullingParamters( float horizontal_fov, 
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance) {

    horizontal_fov_ = horizontal_fov;
    vertical_fov_ = vertical_fov;
    near_plane_distance_ = near_plane_distance;
    far_plane_distance_ = far_plane_distance;

    return 0;
}

void  ObjectLocalizer::SetParamters( double object_height) {
  object_height_ = object_height;
}

int ObjectLocalizer::LoadCalibrationFile(const std::string& calibration_filepath) {
	cv::FileStorage fs;

  fs.open(calibration_filepath, cv::FileStorage::READ);
  if(!fs.isOpened()){
      LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
      return -1;
  }
  
  fs["CameraMat"] >> intrinsic_mat_;
  fs["DistCoeff"] >> distortion_coeffs_;
  fs["ImageSize"] >> image_size_;

  cv::Mat cam_extrinsic_mat;

  fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
  fs.release();   
  cv::cv2eigen(cam_extrinsic_mat, camara_to_map_pose_);

// Note: This assumes a coordinate system where X is forward, Y is up, and Z is right. To convert from the traditional camera coordinate system (X right, Y down, Z forward), one can use:
  Eigen::Matrix4f cam2robot;
  cam2robot << 0, 0, 1, 0,
            0,-1, 0, 0,
            1, 0, 0, 0,
            0, 0, 0, 1;
    camara_to_ref_for_culling_ = camara_to_map_pose_ * cam2robot;

    ref_to_camera_pose_ = camara_to_map_pose_.inverse();

    return 0;
 }

pcl::PointCloud<pcl::PointXYZRGB>::Ptr ObjectLocalizer::CullingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud, const Eigen::Matrix4f& camara_pose) {

  pcl::FrustumCulling<pcl::PointXYZRGB> fc_;
  fc_.setInputCloud (point_cloud);
  fc_.setVerticalFOV (vertical_fov_);
  fc_.setHorizontalFOV (horizontal_fov_);
  fc_.setNearPlaneDistance (near_plane_distance_);
  fc_.setFarPlaneDistance (far_plane_distance_);
  fc_.setCameraPose (camara_pose);

  std::vector<int> filter_indices;
  fc_.filter (filter_indices); 


  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  inliers->indices = move(filter_indices);
  pcl::ExtractIndices<pcl::PointXYZRGB> extract;
  extract.setInputCloud (point_cloud);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*cloud_filtered);

  return cloud_filtered;
}

// static bool upsampling = false;

int ObjectLocalizer::GenerateDistanceImage() {
  if (cloud_map_->empty()) {
    return - 1;
  }
  distance_image_ = cv::Mat(image_size_, CV_32F, numeric_limits<float>::max());
  point3d_image_ = cv::Mat(image_size_, CV_32FC3, cv::Vec3f{numeric_limits<float>::min(),  numeric_limits<float>::min(), numeric_limits<float>::min()});

  Eigen::Matrix4f world_to_camera_transform = ref_to_camera_pose_;

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);

  Eigen::Matrix4f camera_to_world_for_culling = camara_to_ref_for_culling_;
  auto culled_pcl = CullingPointCloud(cloud_map_, camera_to_world_for_culling);
  if (culled_pcl->empty()) {
    return - 1;
  }
  // pcl::io::savePCDFile("cull.pcd", *culled_pcl);

  // if (upsampling) {
  //   auto upsamping_pcl = UpsamplingPointCloud(culled_pcl);
  //   culled_pcl = upsamping_pcl;
  // }
  std::vector<cv::Point3f> points_to_colorize;

  points_to_colorize.reserve(culled_pcl->size());
  for (size_t i = 0; i < culled_pcl->size(); ++i) {
      const auto& point = culled_pcl->at(i);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));
  }

  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  rotation_vec_ = rotation_vec.t();
  transition_vec_ = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
  std::vector<cv::Point2f> image_points;
  world_to_camera.convertTo(world_to_camera, CV_64F);

  cv::projectPoints(points_to_colorize, rotation_vec_, transition_vec_, intrinsic_mat_, distortion_coeffs_, image_points);
  std::vector<cv::Point3f> points_to_camera;
  cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);
  for (size_t i = 0; i < image_points.size(); ++i) {
      float y = image_points[i].y, x = image_points[i].x;
        int col = round(x), row = round(y);
      if (row >= 0 && row < image_size_.height && col >= 0 && col < image_size_.width) {
          float distance = cv::norm(points_to_camera[i]);
          float current_distance = distance_image_.at<float>(row, col);
          if (distance< current_distance)
            distance_image_.at<float>(row, col) = distance;
            point3d_image_.at<cv::Point3f>(row, col) = points_to_colorize[i];


      }
  }

  return 0;

}

int ObjectLocalizer::SetCloudMap(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud_map) {
  cloud_map_ = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
  *cloud_map_ =  *cloud_map;
  return 0;
}


int ObjectLocalizer::Localize(const cv::Rect2f&  object_region, cv::Point3f& object_center, std::vector<cv::Point3f>& object_area) {

  int  result =0; 
  if (distance_image_.empty()) {
    result = GenerateDistanceImage();
    if (0 != result)
      return result;
  }

  cv::Point2f object_position2d ; 
  double object_height;
  GetObjectPosition(object_region, object_position2d, object_height);

  result =  LocalizePoint(object_position2d, object_height, object_center);
  if (0 != result)
    return result;

  return GetOccupiedArea(object_region,  object_center, object_area);
  
}

void ObjectLocalizer::GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position2d, double & object_height) {

  object_position2d = cv::Point2f{object_region.x + object_region.width / 2, object_region.y + object_region.height}; 
  object_height = object_height_;
}

int ObjectLocalizer::LocalizePoint( const cv::Point2f& position_2d, double object_height, cv::Point3f& object_position){
  std::vector< cv::Point2f> image_points = {position_2d};
  std::vector< cv::Point2f> undistorted_points;
  cv::undistortPoints(image_points, undistorted_points, intrinsic_mat_, distortion_coeffs_);
   cv::Point2f undistorted_point = undistorted_points[0];

   Eigen::Vector3f object_to_cam = Eigen::Vector3f(undistorted_point.x, undistorted_point.y, 1);

  Eigen::Isometry3f cam_to_map_transform(camara_to_map_pose_);
  Eigen::Vector3f object_to_world = cam_to_map_transform * object_to_cam;

  Eigen::Vector3f cam_position = camara_to_map_pose_.block<3, 1>(0, 3); 
  Eigen::Vector3f cam_to_object_vec= object_to_world -  cam_position;
  cam_to_object_vec = cam_to_object_vec /cam_to_object_vec.norm();

  float ground_z;
  if (!GetGroundZ(position_2d, ground_z))
    return -1;

  float cam_height = cam_position[2] - ground_z;

  float angle_to_z = atan2(abs(cam_to_object_vec[2]), Eigen::Vector2f(cam_to_object_vec[0], cam_to_object_vec[1]).norm());
  if (angle_to_z < 0.001)
    return -1;
  float cam_to_object_distance = (cam_height - object_height) / sin(angle_to_z);

   Eigen::Vector3f position =  cam_position + cam_to_object_distance *cam_to_object_vec ;
   object_position =  cv::Point3f(position[0], position[1], ground_z);

    return 0;
}

bool ObjectLocalizer::  GetGroundZ(const cv::Point2f& position_2d, float& ground_z) {
  cv::Point3f object_position;
  int result ;
  if (distance_image_.empty()) {
    result = GenerateDistanceImage();
    if (0 != result)
      return false;
  }

 int min_search_pixels = image_size_.height * 0.1;
  int col = round(position_2d.x) , row = round(position_2d.y) ;
  bool found = false;
  ground_z = std::numeric_limits<float>::max();
  for (int y = row; y < image_size_.height; y++) {
    object_position = point3d_image_.at<cv::Point3f>(y, col) ;
    // auto distance = distance_image_.at<float>(y, col) ;
    // get 3d point in poind cloud correspond to the pixel.
    if (object_position.x > MIN_VALID_X) {
      found = true;
      ground_z  = std::min(ground_z, object_position.z);
      if (y >= min_search_pixels + row)  
        break;
    }
  }
  return found;

}

cv::Point2f   ObjectLocalizer::Project3dPointToImage( const cv::Point3f& object_position) {

    Eigen::Matrix4f world_to_camera_transform =  camara_to_map_pose_.inverse();
    cv::Mat world_to_camera;
    // rotatiion matrix convert world coordinate of a point  to camera coordinate 
    cv::eigen2cv(world_to_camera_transform, world_to_camera);
      
    cv::Mat rotation_vec;
    cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
    rotation_vec_ = rotation_vec.t();
    transition_vec_ = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
    std::vector<cv::Point2f> image_points;
     std::vector<cv::Point3f> points_3d= {object_position};
    cv::projectPoints(points_3d, rotation_vec_, transition_vec_, intrinsic_mat_, distortion_coeffs_, image_points);

    return image_points[0];

}
} // namespace object_location
}// namespace object_location
