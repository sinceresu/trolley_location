#pragma once
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/voxel_grid_occlusion_estimation.h>

namespace trolley_location{

namespace object_location{
  
class ObjectLocalizer 
{
public:
  explicit ObjectLocalizer() ;

  virtual ~ObjectLocalizer();

  ObjectLocalizer(const ObjectLocalizer&) = delete;
  ObjectLocalizer& operator=(const ObjectLocalizer&) = delete;

  int SetCloudMap(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud_map);
  int LoadCalibrationFile(const std::string& calibration_filepath) ;
  int SetCullingParamters( float horizontal_fov, 
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance) ;
void SetParamters( double object_height) ;
int Localize( const cv::Rect2f&  object_region, cv::Point3f& object_center, std::vector<cv::Point3f>& object_occupied_area);

cv::Point2f  Project3dPointToImage( const cv::Point3f& object_position);

protected:
int LocalizePoint( const cv::Point2f& object_position_2d, double object_height, cv::Point3f& object_position);

virtual void GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position2d, double & object_height);

 pcl::PointCloud<pcl::PointXYZRGB>::Ptr CullingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud, const Eigen::Matrix4f& parent_link_pose) ;

  int GenerateDistanceImage();
  cv::Point3f AdjustLocation(const cv::Point3f&object_position);
  virtual int GetOccupiedArea(const cv::Rect2f&  object_region, const cv::Point3f&object_center, std::vector<cv::Point3f>& object_occupied_area) = 0;

  // void InitializeOcclusionFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  Eigen::Matrix4f camara_to_map_pose_;
  Eigen::Matrix4f camara_to_ref_for_culling_;
  Eigen::Matrix4f ref_to_camera_pose_;

  cv::Mat rotation_vec_;
  cv::Mat transition_vec_;

  float horizontal_fov_;
  float vertical_fov_;
  float near_plane_distance_; 
  float far_plane_distance_;
  

  cv::Mat distance_image_;
  cv::Mat point3d_image_;


  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_map_;

  double object_height_;

private:

  bool GetGroundZ(const cv::Point2f& ball_center, float& ground_z);


 };
} // namespaceobject_location
}// namespaceobject_location
