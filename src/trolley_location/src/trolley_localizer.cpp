#include "trolley_localizer.h"


using namespace std;

namespace trolley_location{

namespace object_location{
namespace {

const float TROLLEY_WIDTH=  0.5f;

}


TrolleyLocalizer::TrolleyLocalizer() 

{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

TrolleyLocalizer::~TrolleyLocalizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}

int TrolleyLocalizer::GetOccupiedArea(const cv::Rect2f&  object_region, const cv::Point3f&object_center, std::vector<cv::Point3f>& object_occupied_area) {
  object_occupied_area.clear();
  object_occupied_area.push_back(cv::Point3f(object_center.x - TROLLEY_WIDTH, object_center.y - TROLLEY_WIDTH, object_center.z));
  object_occupied_area.push_back(cv::Point3f(object_center.x - TROLLEY_WIDTH, object_center.y + TROLLEY_WIDTH, object_center.z));
  object_occupied_area.push_back(cv::Point3f(object_center.x + TROLLEY_WIDTH, object_center.y - TROLLEY_WIDTH, object_center.z));
  object_occupied_area.push_back(cv::Point3f(object_center.x + TROLLEY_WIDTH, object_center.y + TROLLEY_WIDTH, object_center.z));
  return 0;
}
} // namespace object_location
}// namespace object_location
