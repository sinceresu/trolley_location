#pragma once

#include "object_localizer.h"

namespace trolley_location{

namespace object_location{
  
class ObstructionLocalizer  : public ObjectLocalizer
{
public:
  explicit ObstructionLocalizer() ;

  virtual ~ObstructionLocalizer();

  ObstructionLocalizer(const ObstructionLocalizer&) = delete;
  ObstructionLocalizer& operator=(const ObstructionLocalizer&) = delete;

private:
  virtual int GetOccupiedArea(const cv::Rect2f&  object_region,const cv::Point3f&object_center, std::vector<cv::Point3f>& object_occupied_area) override;


};
} // namespaceobject_location
}// namespaceobject_location
