#include "object_localizer_factory.h"

#include "trolley_localizer.h"
#include "person_localizer.h"
#include "obstruction_localizer.h"

using namespace std;

namespace trolley_location{
namespace object_location{

std::shared_ptr<ObjectLocalizer> ObjectLocalizerFactory::CreateObjectLocalizer(std::string object_label)  {
  if (object_label == "person")
    return std::make_shared<PersonLocalizer>();
  if (object_label == "trolley")
    return std::make_shared<TrolleyLocalizer>();
  return std::make_shared<ObstructionLocalizer>();

}
} // namespace object_location
}// namespace object_location
