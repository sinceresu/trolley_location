#include "person_localizer.h"


using namespace std;

namespace trolley_location{

namespace object_location{
namespace {
const float HALF_BODY_RATIO_DOWN_THRESHOLD= 0.5f;
const float HALF_BODY_RATIO_UP_THRESHOLD= 1.2f;
const float PERSON_HEAD_HEIGHT = 1.7f;
const float EDEG_RATIO=  0.02f;
const float PERSON_WIDTH=  0.3f;

}


PersonLocalizer::PersonLocalizer() 

{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

PersonLocalizer::~PersonLocalizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}

void PersonLocalizer::GetObjectPosition(const cv::Rect2f&  object_region, cv::Point2f&  object_position2d, double & object_height) {
  if (!IsWholeBody(object_region) && IsAtBottom(object_region)) {
    object_position2d = cv::Point2f{object_region.x + object_region.width / 2, object_region.y}; 
    object_height = PERSON_HEAD_HEIGHT;
    return;
  }
  return ObjectLocalizer::GetObjectPosition(object_region, object_position2d, object_height);
}

bool PersonLocalizer::IsWholeBody(const cv::Rect2f& object_region) {
  float ratio = object_region.width / object_region.height;
  return ratio < HALF_BODY_RATIO_DOWN_THRESHOLD;
}

bool PersonLocalizer::IsAtBottom(const cv::Rect2f& object_region) {
  auto position_2d = cv::Point2f{object_region.x + object_region.width / 2, object_region.y + object_region.height}; 
  return position_2d.y > ( 1.0f - EDEG_RATIO) *image_size_.height ;
}

int PersonLocalizer::GetOccupiedArea(const cv::Rect2f&  object_region, const cv::Point3f&object_center, std::vector<cv::Point3f>& object_occupied_area) {
  object_occupied_area.clear();
  object_occupied_area.push_back(cv::Point3f(object_center.x - PERSON_WIDTH, object_center.y - PERSON_WIDTH, object_center.z));
  object_occupied_area.push_back(cv::Point3f(object_center.x - PERSON_WIDTH, object_center.y + PERSON_WIDTH, object_center.z));
  object_occupied_area.push_back(cv::Point3f(object_center.x + PERSON_WIDTH, object_center.y - PERSON_WIDTH, object_center.z));
  object_occupied_area.push_back(cv::Point3f(object_center.x + PERSON_WIDTH, object_center.y + PERSON_WIDTH, object_center.z));

  return 0;
}
} // namespace object_location
}// namespace object_location
