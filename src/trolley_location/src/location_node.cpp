#include "location_node.h"
#include <limits>

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PolygonStamped.h"

#include<pcl/io/pcd_io.h>
#include "glog/logging.h"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Matrix3x3.h"
#include "object_localizer.h"
#include "object_localizer_factory.h"

#include "trolley_location_msgs/ObjectPose.h"

namespace trolley_location {
namespace  object_location{

constexpr int kInfiniteSubscriberQueueSize = 0;
constexpr int kLatestOnlyPublisherQueueSize = 1;

namespace {
// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (LocationNode::*handler)(const std::string&,
                          const typename MessageType::ConstPtr&),
                        const std::string& topic,
    ::ros::NodeHandle* const node_handle, LocationNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kInfiniteSubscriberQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}

}

LocationNode::LocationNode(const LocationNodeOptions& node_options) : node_options_(node_options)
 {
  pose_position_publisher_ = node_handle_.advertise<::trolley_location_msgs::ObjectPose>(
          node_options_.location_topic, kLatestOnlyPublisherQueueSize);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_map= pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>() );
    
  pcl::io::loadPCDFile(node_options_.cloud_map_filepath, *cloud_map);

  for (size_t i = 0;  i <  node_options.object_types.size(); i++) {
    int object_type = node_options.object_types[i];
    pose_localizers_[object_type] = ObjectLocalizerFactory::CreateObjectLocalizer(node_options.object_labels[i]);
      // pose_localizer_ =std::make_shared<RobotLocalizer>();
    if (pose_localizers_[object_type] ->LoadCalibrationFile(node_options_.calibration_filepath) != 0)
      return;
    
    pose_localizers_[object_type] ->SetCullingParamters(node_options_.horizontal_fov, 
      node_options_.vertical_fov, 
      node_options_.near_plane_distance, 
      node_options_.far_plane_distance
    );
    pose_localizers_[object_type] ->SetParamters(node_options_.object_heights[i]); 

    pose_localizers_[object_type]  ->SetCloudMap(cloud_map);
  }


  LaunchSubscribers();

}


LocationNode::~LocationNode() { }

void LocationNode::LaunchSubscribers() {
    detect_subscriber_ = SubscribeWithHandler<trolley_location_msgs::ObjectDetect>(
             &LocationNode::HandleDetectionMessage, node_options_.detect_topic, &node_handle_,
             this);
}

void LocationNode::HandleDetectionMessage(const std::string& sensor_id,
                              const trolley_location_msgs::ObjectDetect::ConstPtr& msg) {
  LOG(INFO) << "HandleDetectionMessage.";
  trolley_location_msgs::ObjectPose object_poses;

  object_poses.header.frame_id = msg->header.frame_id;
  object_poses.header.stamp =   msg->header.stamp;
  // object_poses.cam_id =   msg->cam_id;

  for (size_t i = 0 ; i <msg->polygons.size(); i++) {
    CHECK_EQ(msg->polygons[i].points.size(), 2) << "the number of object region points is not 2!";

    float object_x = msg->polygons[i].points[0].x;
    float object_y = msg->polygons[i].points[0].y;
    float object_width = msg->polygons[i].points[1].x - object_x;
    float object_height = msg->polygons[i].points[1].y - object_y;

    cv::Rect2f object_region(object_x , object_y, object_width, object_height);
    int object_id = msg->object_ids[i];
    int object_type = msg->object_types[i];

    if (pose_localizers_.count(object_type) == 0)  {
      LOG(WARNING) << "Undefined Object type:" << object_type;
      break;
    }

    cv::Point3f object_position3d;
    std::vector<cv::Point3f> object_area;
    if (0 != pose_localizers_[object_type]->Localize(object_region, object_position3d, object_area) ) {
      break;
  }
  // LOG(INFO) << "Localize ok: " << object_position;

  object_poses.object_types.push_back(object_type);

  ::geometry_msgs::Pose pose;
  ::geometry_msgs::Point position;
  position.x = object_position3d.x;
  position.y = object_position3d.y;
  position.z = object_position3d.z;
  pose.position = position;

  object_poses.poses.push_back(pose);

  ::geometry_msgs::Polygon occupied_area;

  for (const  auto & object_corner : object_area) {
    ::geometry_msgs::Point32 corner;
    corner.x = object_corner.x;
    corner.y = object_corner.y;
    corner.z = object_corner.z;
    occupied_area.points.push_back(corner);
  }
  object_poses.occupied_areas.push_back(occupied_area);

  // static std::ofstream out_path("trajectory.txt");
  // out_path << msg->header.stamp << ", " << position;

  LOG(INFO) << "object region: " <<  object_type << std::endl 
  << "x0: " << object_x << std::endl
  << "y0: " << object_y << std::endl
  << "x1: " << (object_x + object_width) << std::endl
  << "y1: " << (object_y + object_height);
  LOG(INFO) << "object position: " <<  object_type << std::endl <<  position << std::endl;
  }
  
  pose_position_publisher_.publish(object_poses);

    
}

}

}
