# trolley location

  trolley mapping  and navigation subsystem in guoanfu zone distribution project.

# Topics
    Object detection module detect ojbjects in 2d images , send  type and  area of detected areas.  Object location module receive object detetion results, calculate s trolley and obstruction  3d position and occupied polygon. 
## Object detection
    /trolley_location/object_detection

## Object Pose
    /trolley_location/object_pose

# Message

## Object detection
sended from Object detection module  to Object location module.
format:
```txt
std_msgs/Header header
string cam_id
int32[] object_ids
int32[] object_types
geometry_msgs/Polygon[] polygons
```
## Object  Pose
sended from Object location module  to routin plan and obstrution module.
format:
```txt
std_msgs/Header header
int32[] object_types
geometry_msgs/Pose[] poses
geometry_msgs/Polygon[] polygons
```